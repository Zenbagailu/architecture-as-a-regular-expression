
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/


import {calculateNormal,calculateFaceNormals} from './utils.js'


export function makeTruncatedPyramid(x,y,z,base,top,h){ //xyz is the centre of the base

  const bx = x - base/2
  const by = y - base/2
  const bz = z

  const tx = x - top/2
  const ty = y - top/2
  const tz = z + h

    return {
    vertices: [
    bx     , by      , bz,
    tx     , ty      , tz,
    bx+base, by      , bz,
    tx+top , ty      , tz,
    bx+base, by+base , bz,
    tx+top , ty+top  , tz,
    bx     , by+base , bz,
    tx     , ty+top  , tz],

    indices: [
    0,4,2, 0,6,4,
    0,3,1, 0,2,3,
    2,5,3, 2,4,5,
    6,7,5, 6,5,4,
    0,7,6, 0,1,7,
    1,5,7, 1,3,5
]
  }

}

export function makeBox(x,y,z,w,l,h){

    return {
    vertices: [
    x  ,y  ,z,
    x  ,y  ,z+h,
    x+w,y  ,z,
    x+w,y  ,z+h,
    x+w,y+l,z,
    x+w,y+l,z+h,
    x  ,y+l,z,
    x  ,y+l,z+h],

    indices: [
    0,4,2, 0,6,4,
    0,3,1, 0,2,3,
    2,5,3, 2,4,5,
    6,7,5, 6,5,4,
    0,7,6, 0,1,7,
    1,5,7, 1,3,5
]
  }

}

//profile is a set of points in x and y, which will be read as 
//cylindrical coordinates at angle 0, as radius and height, respectively.
//The points need to be monotone in y, that is, y has to increase or decrease
//or be the same in the sequence of points, but never reverse direction (the profile
//cannot go back). This is because of the triangulation of the sides.
export function generateElementMeshes(profile, segments){ 



  //make sure y is ordered from smallest to largest
  // if(profile[0][1]>profile[profile.length-1][1]){
  //   profile = profile.reverse()
  // }

  let minx = profile[0][0]
  let maxx = profile[0][0]

  let miny = profile[0][1]
  let maxy = profile[0][1]

  profile.forEach( pt =>{
    minx = minx < pt[0] ? minx : pt[0]; 
    maxx = maxx > pt[0] ? maxx : pt[0];
    miny = miny < pt[1] ? miny : pt[1]; 
    maxy = maxy > pt[1] ? maxy : pt[1];
  })

  let scale = 1 
  if(maxx>1.0){
    scale = 1/(maxx)
  }
  const  nProfile = profile.map(pt => [pt[0]*scale,pt[1]*scale]) //scale x and y equally. the maxx will be one  

  //we check here if the last and first points are the same, that is, if the curve is closed. In that case,
  //we mark it, remove the last point, and deal with the situation accordingly (not adding caps, and closing)
  //the meshes
  let closed = false;
  if(nProfile[0].every((v,i)=> v === nProfile[nProfile.length-1][i])){
    console.log('profile is closed')
    closed = true
    nProfile.pop()
  } 

  const cl = nProfile.length
  const angStep = Math.PI/(2*segments)

   //we only need to calculate normals for those cases where the default won't be correct,
   //that is revolutionMesh(), composedMesh() (in "degenerate" cases) and mirroredRevolutionMesh(), 
   //In these the tangents at the end of the circular segments won't correspond with the faces they
   //will meet when composed, and create a sharper edge in the shading. 
   //Also, now these are fragments of a solid, they cannot be rendered by themselves

    const half = angStep/2
    const rad = 1.0/Math.cos(half)

   function revolutionMesh(){
    const vertices = []
    const indices = []

    //vertices on surfaces. These are calculated not as circumscribed in a circle, but as inscribing the circle
    //(approaching the circular form from the outside). They also start with half an angle, and thus the offset.
    //the formula becomes clear if drawn.

    //begin
    nProfile.forEach(pt=> vertices.push(pt[0]-1.0, -1.0, pt[1]))
    //middle points
    for(let i=1; i <= segments; ++i){
      nProfile.forEach(pt=> vertices.push(rad*pt[0]*Math.cos(i*angStep-half)-1.0,rad*pt[0]*Math.sin(i*angStep-half)-1.0,pt[1]))
    }
    //end
    nProfile.forEach(pt=> vertices.push(-1.0,pt[0]-1.0,pt[1]))


    //main sweep 
    for (let i=0; i <= segments; ++i){
      const bottomA = i*cl
      const bottomB = (i+1)*cl
      for(let j=0; j < cl-1; ++j){
        indices.push(bottomA+j  ,bottomB+j,bottomB+j+1)
        indices.push(bottomA+j+1,bottomA+j,bottomB+j+1)
      }
    }

    if(closed){
      for (let i=0; i <= segments; ++i){
        const fA = i*cl
        const fB = (i+1)*cl
        const bA = (i+1)*cl-1
        const bB = (i+2)*cl-1
        indices.push(
          fA, bA, bB,
          fA, bB, fB)
      }
    }
    else{
      //bottom
      let last = vertices.length/3
      vertices.push(-1.0,-1.0,nProfile[0][1]) 

      for (let i=0; i <= segments; ++i){
        indices.push(last, (i+1)*cl, i*cl)
      } 
      //top
      last = vertices.length/3
      vertices.push(-1.0,-1.0,nProfile[nProfile.length-1][1]) 

      for (let i=0; i <= segments; ++i){
        indices.push(last,(i+1)*cl-1, (i+2)*cl-1)
      } 
    }

    return {name:'revolution', vertices: vertices, indices: indices}
  }

  function extrusionMesh(){
    const vertices = []
    const indices = []
  
    //corner one
    nProfile.forEach(pt =>vertices.push(pt[0]-1.0,-1.0,pt[1]))
    //other
    nProfile.forEach(pt =>vertices.push(pt[0]-1.0,1.0,pt[1]))

    //main surface
    for(let i=0; i < cl-1; ++i){
      indices.push(
        i,i+cl  ,i+cl+1,
        i,i+cl+1,i+1,
        )
    }

    if(closed){  
      const fA = 0
      const fB = cl
      const bA = cl-1
      const bB = 2*cl-1
      indices.push(
        fA, bA, bB,
        fA, bB, fB)  
    }
    else{
      //bottom
      const v0 = vertices.length/3
      const v1 = v0+1
      vertices.push(
        -1.0,-1.0,nProfile[0][1],
        -1.0, 1.0,nProfile[0][1])

      indices.push(
        v0, cl, 0,
        v1, cl, v0)

      //top
      const v2 = vertices.length/3
      const v3 = v2+1

      vertices.push(
        -1.0,-1.0,nProfile[nProfile.length-1][1],
        -1.0, 1.0,nProfile[nProfile.length-1][1])

      indices.push(
        v2, cl-1  , 2*cl-1,
        v3, v2, 2*cl-1)
     }

    return {name:'simple', vertices: vertices, indices: indices}
  }

  function composedMesh(){
    const vertices = []
    const indices = []

    //begin
    nProfile.forEach(pt=> vertices.push( pt[0] -1.0, -1.0, pt[1]))

    //main sweep
    for(let i=1; i <= segments; ++i){
      nProfile.forEach(pt=> vertices.push(
         1.0 - rad*(2.0-pt[0])*Math.cos(i*angStep-half),
        -1.0 + rad*(2.0-pt[0])*Math.sin(i*angStep-half),
        pt[1]))
    }

    //end
    nProfile.forEach(pt=> vertices.push(1.0, 1.0-pt[0], pt[1]))


    //indices main sweep 
    for (let i=0; i <= segments; ++i){
      const bottomA = i*cl
      const bottomB = (i+1)*cl
      for(let j=0; j < cl-1; ++j){
        indices.push(bottomA+j,   bottomB+j, bottomB+j+1)
        indices.push(bottomA+j+1, bottomA+j, bottomB+j+1)
      }
    }

    if(closed){
      for (let i=0; i <= segments; ++i){
        const fA = i*cl
        const fB = (i+1)*cl
        const bA = (i+1)*cl-1
        const bB = (i+2)*cl-1
        indices.push(
          fA, bA, bB,
          fA, bB, fB)
      }
    }
    else{
      //bottom
      const v0 = vertices.length/3
      const v1 = v0+1
      const v2 = v0+2
      vertices.push(
           1.0,  1.0, nProfile[0][1],
          -1.0,  1.0, nProfile[0][1],
          -1.0, -1.0, nProfile[0][1])

      //from v1 to all vertices in sweep (bottom)
      for (let i=0; i <= segments; ++i){
        indices.push(v1, (i+1)*cl, i*cl)
      } 

      indices.push( //the two ears
        v1,  0,  v2,
        (segments+1)*cl,v1, v0)

      //top
      const v3 = vertices.length/3
      const v4 = v3+1
      const v5 = v3+2
      vertices.push(
           1.0,  1.0, nProfile[nProfile.length-1][1],
          -1.0,  1.0, nProfile[nProfile.length-1][1],
          -1.0, -1.0, nProfile[nProfile.length-1][1])

      //from v1 to all vertices in sweep (top)
      for (let i=0; i <= segments; ++i){
        indices.push(v4, (i+1)*cl-1, (i+2)*cl-1)
      } 

      indices.push( //the two ears
        cl-1, v4, v5,
        (segments+2)*cl-1, v3, v4)
    }

    return {name:'threequaters', vertices: vertices, indices: indices}
  }


  function mirroredRevolutionMesh(){
    const {vertices, indices} = revolutionMesh()
   
    //rotate all vertices 180 degrees
    const vct = vertices.length
    for(let i = 0; i< vct; i+=3){
      vertices.push(-vertices[i  ],-vertices[i+1],vertices[i+2])
    }
    //add indices to correspond to added, rotated points
    
    indices.push(...indices.map(index => index + vct/3))


    return {name:'mirrored',vertices: vertices, indices: indices}
  }


  function filled(){

    //If the first and last are not the same, that is, if it is not a closed line
    if(closed){
      return {
        name:'full',
        vertices: [],
        indices: []}
    }
    else{
      return {
      name:'full',
      vertices: [
     //bottom
       1.0,  1.0, nProfile[0][1],
      -1.0,  1.0, nProfile[0][1],
      -1.0, -1.0, nProfile[0][1],
       1.0, -1.0, nProfile[0][1],
       //top
       1.0,  1.0, nProfile[nProfile.length-1][1],
      -1.0,  1.0, nProfile[nProfile.length-1][1],
      -1.0, -1.0, nProfile[nProfile.length-1][1],
       1.0, -1.0, nProfile[nProfile.length-1][1]], 
      indices: [
      0,3,1, 1,3,2, //bottom
      4,5,7, 5,6,7 //top
      ]}
    }
  }


  // return {meshes: [revolutionMesh(),extrusionMesh(),composedMesh(),mirroredRevolutionMesh(),filled()]}
  return {
    singleCorner:revolutionMesh(), 
    consecutiveCorners: extrusionMesh(),
    threeCorners: composedMesh(),
    twoOppositeCorners: mirroredRevolutionMesh(),
    fourCorners: filled()}
}
