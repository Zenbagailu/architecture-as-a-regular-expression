
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

// These are necessary to specify for mobile devices (low default values): 
// https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_best_practices

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

void main(){

	gl_FragColor = vec4(0,0,0,1);

  // //Simple rendering for debugging
  // if(gl_FrontFacing) {
  //   gl_FragColor = vec4(0,1,0,1);  // useful color for debugging.
  // } else {
  //   gl_FragColor = vec4(1,0,0,1); // useful color for debugging.
  // }

  // Another useful rendering for debugging
  // they need to be set to corresponding values
  // https://learnopengl.com/Advanced-OpenGL/Depth-testing
  // float near = 0.1;
  // float far  = 5.0;
  // // float near = gl_DepthRange.near;
  // // float far = gl_DepthRange.far;
  // float z = gl_FragCoord.z * 2.0 - 1.0; // back to NDC 
  // float linearDepth = (2.0 * near * far) / (far + near - z * (far - near));	
  // float depth = linearDepth/far;
  // gl_FragColor = vec4(vec3(depth), 1.0);

}