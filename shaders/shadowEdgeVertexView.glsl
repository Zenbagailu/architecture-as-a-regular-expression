
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

precision highp float;
attribute vec4 vertexPosition;
attribute vec3 normal0, normal1;

uniform vec3 lightDir;
uniform mat4 objectMatrix; //this is used to specific transformations 
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

// This code is based on Mikola Lysenko https://github.com/mikolalysenko for stencil buffer 
// shadows using webgl. It can be found here:
// https://github.com/stackgl/webgl-workshop/tree/master/exercises/stencil-shadows
// This its how it works:
// Every vertex processed here corresponds to a triangle sent for an edge in the mesh. Each 
// edge will consist of two triangles, as below. For each vertex in the face (p0, p1, or the 
// 0 vertex passed instead of the vertex not in the edge), there will be two normals 
// associated, those of the two faces incident to the edge. These are used to determine if 
// the edge is in the shadow edge or not. If it isn't, 0 vertices will be emitted, which 
// won't be drawn. If it is a shadow edge, the fourth w component passed in this case (1)
// will mean that they are passed as they are (observe that the 1.0-vertexPosition.w will be 0,
// so it won't add anything to vertexPosition). If it is the extra vertex 0,0,0,0, it will be 
// processed and passed as the direction (lightDir.x,lightDir.y,lightDir.z, 0). A triangle
// Drawn with a direction (a point at infinitude) it will have its two sides converging at 
// this point at infinitude parallel.
//
//
//                                  / \
//                                /    \
//                              /   n1  \
//                            p0 --------p1
//                              \   n0  / 
//                               \    /
//                                \ / 
//
// For each edge (p0, p1) with n0 to the right and n1 to the left, the following sequence 
// of coordinates for each edge will be constructed:
//  
//
//   p0, n0,n1, p1,n0,n1, 0, n0, n1,    p1, n1,n0,  p0, n1,n0, 0 , n1,n0,
//
//                                         n1
//		     0
//        / \      					          p0<-------p1
//      /    \     					           \   n0  / 
//    /   n1  \    					            \    /
//  p0 ------->p1  					             \ / 
//                                        0
//       n0
//
//
//  p0 and p1 are given in all cases a w coordinate of 1, and 0 (0,0,0,0) of 0. 



void main() {
  if(dot(normalize((modelViewMatrix * objectMatrix * vec4(normal0,0)).xyz), lightDir) < 0.0 &&
     dot(normalize((modelViewMatrix * objectMatrix * vec4(normal1,0)).xyz), lightDir) >= 0.0) {
    gl_Position = projectionMatrix * ((modelViewMatrix * objectMatrix * vertexPosition) + vec4((1.0-vertexPosition.w) * lightDir, 0.0));
  } else {
    gl_Position = vec4(0,0,0,0);
  }
}
