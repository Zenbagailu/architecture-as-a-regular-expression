
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

precision highp float;
attribute vec3 vertexPosition;
attribute vec3 vertexNormal;

uniform mat4 objectMatrix; //this is used to specific transformations 
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

varying vec3 normalInterp;
varying vec3 vertPos;

void main(){
	mat4 composedMatrix =   modelViewMatrix * objectMatrix;
  	vec4 vertPos4 = composedMatrix * vec4(vertexPosition, 1.0);
  	vertPos = vec3(vertPos4) / vertPos4.w;
  	
  	// Transpose and inverse don't work in glsl es 2.0. This for of transforming the
  	// normal is necessary only if non-uniform transformations are applied 
  	// (non-uniform scaling, shearing, etc. See: 
  	// https://learnopengl.com/Lighting/Basic-Lighting). 
  	// So an alterantive is not to use any of these transformations. Another one is 
  	// to calculate the matrix outside and have it as a uniform. Lastly, a third 
  	// alternative is to implement the calculations here.
  	//normalInterp = mat3(transpose(inverse(composedMatrix))) * vertexNormal; 
  	//normalInterp = vec3(normalMatrix * vec4(vertexNormal, 0.0));//second alternative
  	normalInterp = normalize((composedMatrix * vec4(vertexNormal,0)).xyz); 
  	gl_Position = projectionMatrix * vertPos4;
}
