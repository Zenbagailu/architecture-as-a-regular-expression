
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/


#define PI  3.1415926536
#define HPI 1.5707963268 //half Pi

precision highp float;
varying vec3 normalInterp;  // Surface normal
varying vec3 vertPos;       // Vertex position

uniform vec4 color;
uniform vec3 lightDir;
uniform vec3 ambient;

float limAng = PI/3.0; //60 degs


void main() {
  vec3 tnorm = normalize(normalInterp);

  float dot= dot(tnorm, -lightDir);

  float limCos = cos(limAng);

  if(dot < 0.0){
  	dot = 0.0;
  }
  else if (dot > limCos){
  	dot = 1.0;
  }
  else{
  	float angle = acos(dot);
  	dot = cos(HPI*(angle-limAng)/(HPI-limAng));
  }

  gl_FragColor = vec4(ambient + dot*color.xyz,color.w);
}
