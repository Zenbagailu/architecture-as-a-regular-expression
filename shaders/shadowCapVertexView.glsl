
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

precision highp float;
attribute vec3 vertexPosition, vertexNormal;

uniform vec3 lightDir;
uniform mat4 objectMatrix; //this is used to specific transformations 
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

// This code is  based on Mikola Lysenko https://github.com/mikolalysenko for stencil buffer 
// shadows using webgl. It can be found here:
// https://github.com/stackgl/webgl-workshop/tree/master/exercises/stencil-shadows
// This is how it works:
// Vertices of triangles from the mesh are passed here, with an associated vertexNormal 
// (for the face it is being drawn). If it is in front of the light (front cap) it is 
// drawn normally; if it is in the back, it is projected to the limits of the view 
// volume (with extend). The projection method has been rewritten so it works with perspective
// projections instead. (The original code by Lysenko worked with parallel projection)

vec4 extend(vec3 p) {
  vec3 tp  = (modelViewMatrix * objectMatrix * vec4(p,1)).xyz; 
  vec3 tld = normalize(lightDir); 

  // No need to draw a cap when light direction is parallel to view plane. 
  // The calculation would fail because it would divide by 0, obviously, 
  // but we could instead calculate intersection with some other plane(for 
  // example one of the viewing frustum planes). This may be necessary
  //in Some cases.
  
  if(tld.z == 0.0){ 
    return vec4(0.0,0.0,0.0,0.0);
  }

  float M22 = projectionMatrix[2][2];
  float M23 = projectionMatrix[3][2];

  float far  = M23/(M22+1.0);
  float near = M23/(M22-1.0);

  float df = (-far  - tp.z)/tld.z;
  float dn = (-near - tp.z)/tld.z;
    //This does not work when part of the mesh is behind the far clipping plane though.
    //the problem is not trivial, so rather make sure the mesh never goes out....
  return df >= 0.0 ? vec4(tp+ 0.9999*df*tld,1.0) : vec4(tp+0.9999*dn*tld,1.0);

}

void main() {
  if(dot(normalize((modelViewMatrix * objectMatrix * vec4(vertexNormal,0)).xyz),lightDir) < 0.0) {
    gl_Position = projectionMatrix * modelViewMatrix * objectMatrix * vec4(vertexPosition,1);
  } else {
    gl_Position =  projectionMatrix * extend(vertexPosition);
  }
}
