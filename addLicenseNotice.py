#!/usr/bin/python3
import os
import sys
import re

program_name = "Architecure as a regular expresions"
copyright_notice = "Copyright (c) 2021 Pablo Miranda Carranza"

gplv3_text = """This file is part of Foobar.
Foobar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Foobar is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
"""

mit_text ="""MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE."""

start_string = '\n/*\n'+ '='*80 +'\n'
end_string = '\n'+ '='*80 +'\n*/\n\n'

#GPL3
# notice_text = copyright_notice + '\n\n' + gplv3_text.replace('(at your option)', '').replace('Foobar', program_name)

#MIT
notice_text = copyright_notice + '\n\n' + mit_text


notice_text = start_string + notice_text + end_string

print(notice_text)

current_dir = os.getcwd()
# exclude = set(['glutess',".git","build"]) #more directories can be added
exclude = set([]) #more directories can be added
for root, dirs, files in os.walk(current_dir, topdown=True):
    #this prunes the directories to exclude: 
    #https://stackoverflow.com/questions/19859840/excluding-directories-in-os-walk
    dirs[:] = [d for d in dirs if d not in exclude]

    for name in files:
         if name.lower().endswith(('.h', '.cpp', '.c', '.glsl', '.js')):
            filename = os.path.join(root, name)
            with open(filename, 'r') as original: 
                data = original.read()
                #if previous notice found, remove it.
                #(?s) is needed to activate the dotall modifier to match new lines 
                data = re.sub(rf"(?s){re.escape(start_string)}(.*?){re.escape(end_string)}", "", data)
            with open(filename, 'w') as modified:
                modified.write(notice_text + data)
                # modified.write(data) #this just deletes previous notices
