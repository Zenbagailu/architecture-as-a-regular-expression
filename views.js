
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

//------------------------------------------------------------------------------
//Some utilities:
//------------------------------------------------------------------------------

//Based on code from glu:
//http://www.opengl.org/wiki/GluProject_and_gluUnProject_code
export function unProject(scPos,modelMatrix,projMatrix,viewport){

	// Calculation for inverting a matrix, compute projection x modelview
    // and store in A
    const TrM = mat4.multiply(mat4.create(),projMatrix, modelMatrix)
    const invTrM = mat4.invert(mat4.create(),TrM)
    if(!invTrM){
    	return null
    }

    const inp = vec4.fromValues(
    (scPos[0]-viewport[0])/viewport[2]*2.0-1.0,
    (scPos[1]-viewport[1])/viewport[3]*2.0-1.0,
    2.0*scPos[2]-1.0,
    1.0)

    const out = vec4.transformMat4(vec4.create(), inp, invTrM)

    return [out[0]/out[3],out[1]/out[3],out[2]/out[3]]
}

//It returns screen coordinates in clip space, or Normalised Device Coordinates (NDC)
export function projectNDC(modelPos,modelMatrix,projMatrix){

	const TrM = mat4.multiply(mat4.create(),projMatrix, modelMatrix)
	const out = vec4.transformMat4(vec4.create(), vec4.fromValues(modelPos[0],modelPos[1],modelPos[2],1), TrM)
	if (out[3] === 0){
		return null
	}
	return [out[0]/out[3],out[1]/out[3],out[2]/out[3],1.0]
}


export function project(modelPos,modelMatrix,projMatrix,viewport){
	const ndc = projectNDC(modelPos,modelMatrix,projMatrix)
	return [viewport[0] + viewport[2]*(ndc[0] +1)/2.0, viewport[1] + viewport[3]*(ndc[1] +1)/2.0,(ndc[2]+1)/2.0]
}


//----------------------------------------------------------------------------------------------
//PERSPECTIVEVIEW 
//----------------------------------------------------------------------------------------------

export function PerspectiveView (
	zNear = 0.1,
	zFar = 1000,
	distance = 1.00,
	foV = 30,
	angleAxisX = 0, 
	angleAxisY = 0,
	center = [0,0,0])
{

	this.angleAxisX = angleAxisX
    this.angleAxisY = angleAxisY
	this.center = center
	this.zNear = zNear
	this.zFar = zFar
	this.distance = distance
	this.foV = foV

	this.projectionMatrix = mat4.create()
	this.modelViewMatrix = mat4.create()
	this.updateModelView()

}

PerspectiveView.prototype.resize = function(width,height){

	this.width = width
	this.height = height

	mat4.perspective(
		this.projectionMatrix,
		this.foV,
		width/height, //aspect
		this.zNear,
		this.zFar)
}

PerspectiveView.prototype.set = function(gl, programInfo){
  gl.useProgram(programInfo.program)

  gl.uniformMatrix4fv(
      programInfo.uniformLocations.projectionMatrix,
      false,
      this.projectionMatrix)

  gl.uniformMatrix4fv(
      programInfo.uniformLocations.modelViewMatrix,
      false,
      this.modelViewMatrix)
}

PerspectiveView.prototype.updateModelView = function(){ //apply transformations

	mat4.translate(
	this.modelViewMatrix,     		// destination matrix
	mat4.create(),    			   	// matrix to translate
	[ 0.0, 0.0, -this.distance])  	// default position

	mat4.rotateX(this.modelViewMatrix,this.modelViewMatrix,this.angleAxisX) 
	mat4.rotateY(this.modelViewMatrix,this.modelViewMatrix,this.angleAxisY)

	mat4.rotateX(this.modelViewMatrix,this.modelViewMatrix,Math.PI/2) //+Math.PI/2 to have z axis pointing upwards
	
	mat4.translate(
	this.modelViewMatrix,     // destination matrix
	this.modelViewMatrix,     // matrix to translate
	[-this.center[0],-this.center[1],-this.center[2]]) 
}


PerspectiveView.prototype.rotate = function(dir){
	this.angleAxisX -=   Math.PI * dir[1]/this.height //half
	this.angleAxisY -= 2*Math.PI * dir[0]/this.width
	const vLim = Math.PI/2-0.001
	this.angleAxisX = this.angleAxisX >= vLim ? vLim : this.angleAxisX < -vLim ? -vLim: this.angleAxisX
	this.updateModelView()
}

PerspectiveView.prototype.zoom = function(zoom){
	this.distance *= (1 + 2 * zoom / this.height)
	this.distance = this.distance <= this.zNear? this.zNear : this.distance >= this.zFar ?  this.zFar : this.distance
	this.updateModelView()
}

PerspectiveView.prototype.pan = function(posA, posB){
	this.updateModelView() //needs all transformations applied...

	const viewport= [0, 0, this.width, this.height]

	// the ys of the positions need to be recalculated, since the origin is placed at
	// the bottom left of the element:
	posA[1] = this.height - posA[1]
	posB[1] = this.height - posB[1]

	const projA = unProject(posA.concat([0]),this.modelViewMatrix,this.projectionMatrix,viewport)
	let    dirA = unProject(posA.concat([1]),this.modelViewMatrix,this.projectionMatrix,viewport)
	dirA = dirA.map( (x,i) => x - projA[i] )

	const projB = unProject(posB.concat([0]),this.modelViewMatrix,this.projectionMatrix,viewport)
	let    dirB = unProject(posB.concat([1]),this.modelViewMatrix,this.projectionMatrix,viewport)
	dirB = dirB.map( (x,i) => x - projB[i] )

	//this defines in model space the planes parallel to the viewing plane
	const viewCenter =[this.width/2, this.height/2] //centre of screen
	const projC = unProject(viewCenter.concat([0]),this.modelViewMatrix,this.projectionMatrix,viewport)
	const  dirC = unProject(viewCenter.concat([1]),this.modelViewMatrix,this.projectionMatrix,viewport)
	const planeNormal = dirC.map( (x,i) => x - projC[i] )

	//now we calculate intersections(from solving line-plane intersection equations)
	//for a plane parallel to the view plane through center, and the rays from the two screen
	//positions perpendicular to the viewing plane

	const pd=vec3.dot(planeNormal, this.center)

	const fa=vec3.dot(planeNormal, projA)
	const ga=vec3.dot(planeNormal, dirA)
	const gp=(pd-fa)/ga

	//pptA = gp*dirA+projA
	const pptA = vec3.add(vec3.create(),vec3.scale(vec3.create(),dirA,gp), projA)

	const fb=vec3.dot(planeNormal, projB)
	const gb=vec3.dot(planeNormal, dirB)
	const gn=(pd-fb)/gb

	//pptB=gn*dirB+projB
	const pptB= vec3.add(vec3.create(),vec3.scale(vec3.create(),dirB,gn), projB)

	// //because the translation is in the opposite direction than the center, it needs to be subtracted
	this.center = this.center.map( (x,i) => x - (pptB[i] - pptA[i]) )

	this.updateModelView()
}

//----------------------------------------------------------------------------------------------
//ORTHOVIEW 
//----------------------------------------------------------------------------------------------

export function OrthoView (
	zNear = 0.1,
	zFar = 1000,
	zoomVal = 100.00,
	angleAxisX = 0, 
	angleAxisY = 0,
	center = [0,0,0])
{

	this.angleAxisX = angleAxisX
    this.angleAxisY = angleAxisY
	this.center = center
	this.zNear = zNear
	this.zFar = zFar
	this.zoomVal = zoomVal

	this.projectionMatrix = mat4.create()
	this.modelViewMatrix = mat4.create()
	this.updateModelView()

}

OrthoView.prototype.resize = function(width,height){
	this.width = width
	this.height = height

	mat4.ortho(
		this.projectionMatrix,
		-width/(2*this.zoomVal),width/(2*this.zoomVal),
		-height/(2*this.zoomVal),height/(2*this.zoomVal), 
		this.zNear,
		this.zFar)
}

OrthoView.prototype.set = function(gl, programInfo){
  gl.useProgram(programInfo.program)

  gl.uniformMatrix4fv(
      programInfo.uniformLocations.projectionMatrix,
      false,
      this.projectionMatrix)

  gl.uniformMatrix4fv(
      programInfo.uniformLocations.modelViewMatrix,
      false,
      this.modelViewMatrix)
}

OrthoView.prototype.updateModelView = function(){ //apply transformations

	// this.modelViewMatrix = mat4.create()
	mat4.translate(
	this.modelViewMatrix,     		// destination matrix
	mat4.create(),    			   	// matrix to translate
	[ 0.0, 0.0, -(this.zFar-this.zNear)/2])  	// default position


	mat4.rotateX(this.modelViewMatrix, this.modelViewMatrix , this.angleAxisX)
	mat4.rotateY(this.modelViewMatrix, this.modelViewMatrix , this.angleAxisY)
	mat4.rotateX(this.modelViewMatrix,this.modelViewMatrix,-Math.PI/2) //+Math.PI/2 to have z axis pointing upwards
	mat4.translate(this.modelViewMatrix, this.modelViewMatrix , this.center.map(p => -p))
}


OrthoView.prototype.rotate = function(dir){
	this.angleAxisX -=   Math.PI * dir[1]/this.height //half
	this.angleAxisY -= 2*Math.PI * dir[0]/this.width
	this.updateModelView()
}

OrthoView.prototype.zoom = function(zoom){
	//This is a bit of a hack... but it works.
	const nzoom = this.zoomVal * (1 + zoom/this.height)
	if(nzoom > 0 && nzoom <= 283700){ //the limit has been experimentally determined (hacked).
		this.zoomVal = nzoom
	}
	mat4.ortho(
		this.projectionMatrix,
		-this.width/(2*this.zoomVal),this.width/(2*this.zoomVal),
		-this.height/(2*this.zoomVal),this.height/(2*this.zoomVal), 
		this.zNear,
		this.zFar)
	// console.log(this.zoomVal)
}

OrthoView.prototype.pan = function(posA, posB){
	// console.log(`${posB}`)
	this.updateModelView() //needs all transformations applied...
	// the ys of the positions need to be recalculated, since the origin is placed at
	// the bottom left of the element:
	posA[1] = this.height - posA[1]
	posB[1] = this.height - posB[1]

	const viewport= [0, 0, this.width, this.height]

	const projA = unProject(posA.concat([0]),this.modelViewMatrix,this.projectionMatrix,viewport)
	const projB = unProject(posB.concat([0]),this.modelViewMatrix,this.projectionMatrix,viewport)

	// //because the translation is in the opposite direction than the center, it needs to be subtracted
	this.center = this.center.map( (x,i) => x - (projB[i] - projA[i]) )
	this.updateModelView()
	// console.log(this.center)
}



//----------------------------------------------------------------------------------------------
//NAVIGATION3D 
//----------------------------------------------------------------------------------------------

export function Navigation3D(view) {
	// this.view = view

	let button = -1 //invalid

	let lastPressX
	let lastPressY

	// let xRotation = Math.PI / 20
	// let yRotation = 0

	let thisnav3d = this //don't mess with this and closures.

	const devicePixelRatio = window.devicePixelRatio || 1; //for dealing with retina displays, etc

	this.startMove = function(x,y,startButton){
		button = startButton
		lastPressX = x
		lastPressY = y
	}

	this.processMove = function(x,y){
		if(button === 0){
			view.rotate([(x-lastPressX)*devicePixelRatio, (y-lastPressY)*devicePixelRatio])
			//view.rotate([(x-lastPressX), (y-lastPressY)]) //this works better
		}else if(button === 1){ //alternative zoom to wheel scroll
			 view.zoom((lastPressY-y)*devicePixelRatio)
		}else if(button === 2){
			// console.log(`panning: ${x}, ${y}`)
			if(view.pan !== undefined){
				view.pan([lastPressX*devicePixelRatio, lastPressY*devicePixelRatio],[x*devicePixelRatio, y*devicePixelRatio])
			}
		}

		lastPressX = x
		lastPressY = y
    }

	this.onmousedown = function(evt) {
		thisnav3d.startMove(evt.pageX,evt.pageY,evt.button)
	}

	this.onmouseup = function() {
		button =-1
	}

	this.onmouseout = function() {
		button =-1
	}

	this.onmousemove = function(evt) {
		evt.preventDefault()
		thisnav3d.processMove(evt.pageX,evt.pageY)
	}

	this.onwheel = function(evt) {
		view.zoom(evt.deltaY);
	}

	// touch events
	this.touchStart = function(evt) {
		 //the number of fingers involved (0 is right button).  Only coordinates of firs finger used
		thisnav3d.startMove( evt.touches[0].clientX,evt.touches[0].clientY,evt.touches.length-1)
	}

	this.touchMove = function(evt) {
		evt.preventDefault()
		thisnav3d.processMove(evt.touches[0].clientX, evt.touches[0].clientY)
	}

	this.touchEnd = function(evt){
		button =-1
	}

	this.addListenersTo = function(canvas){
		canvas.addEventListener('mousedown',this.onmousedown)
		canvas.addEventListener('mouseup',this.onmouseup)
		canvas.addEventListener('mouseout',this.onmouseout)
		canvas.addEventListener('mousemove',this.onmousemove)
		canvas.addEventListener('wheel', this.onwheel)
		canvas.addEventListener('touchstart', this.touchStart)
		canvas.addEventListener('touchmove', this.touchMove)
		canvas.addEventListener('touchend', this.touchEnd)
	}
}

//----------------------------------------------------------------------------------------------
//FIXED PERSPECTIVE: Prespective without pan.
//----------------------------------------------------------------------------------------------

export function FixedPerspective(
	zNear = 0.1,
	zFar = 1000,
	closeLim = 0.1, //these limits are for the zoom. Because shadows have problems when on zNear of zFar
	farLim = 1000,
	distance = 1.00,
	foV = 30,
	angleAxisX = 0, 
	angleAxisY = 0,
	center = [0,0,0])
{

	let projectionMatrix = mat4.create()
	let modelViewMatrix = mat4.create()

	let button = -1 //invalid
	let lastPressX, lastPressY

	let thisnav3d = this //don't mess with this and closures.

	const devicePixelRatio = window.devicePixelRatio || 1; //for dealing with retina displays, etc

	this.resize = function(width,height){

		this.width = width
		this.height = height

		mat4.perspective(
			projectionMatrix,
			foV,
			width/height, //aspect
			zNear,
			zFar)
	}

	this.set = function(gl, programInfo){
	  gl.useProgram(programInfo.program)

	  gl.uniformMatrix4fv(
	      programInfo.uniformLocations.projectionMatrix,
	      false,
	      projectionMatrix)

	  gl.uniformMatrix4fv(
	      programInfo.uniformLocations.modelViewMatrix,
	      false,
	      modelViewMatrix)
	}

	this.updateModelView = function(){ //apply transformations

		mat4.translate(
		modelViewMatrix,     		// destination matrix
		mat4.create(),    			   	// matrix to translate
		[ 0.0, 0.0, -distance])  	// default position

		mat4.rotateX(modelViewMatrix,modelViewMatrix,angleAxisX) 
		mat4.rotateY(modelViewMatrix,modelViewMatrix,angleAxisY)

		mat4.rotateX(modelViewMatrix,modelViewMatrix,Math.PI/2) //+Math.PI/2 to have z axis pointing upwards
		
		mat4.translate(
		modelViewMatrix,     // destination matrix
		modelViewMatrix,     // matrix to translate
		[-center[0],-center[1],-center[2]]) 
	}

	this.updateModelView() //it needs to be called after declaration


	this.rotate = function(dir){
		angleAxisX -=   Math.PI * dir[1]/this.height //half
		angleAxisY -= 2*Math.PI * dir[0]/this.width
		const vLim = Math.PI/2-0.001
		angleAxisX = angleAxisX >= vLim ? vLim : angleAxisX < -vLim ? -vLim: angleAxisX
		this.updateModelView()
	}

	this.zoom = function(zoom){
		distance *= (1 + 2 * zoom / this.height)
		distance = distance <= closeLim ? closeLim : distance >= farLim ?  farLim : distance
		this.updateModelView()
	}
}

//----------------------------------------------------------------------------------------------
//FIXED ORTHO VIEW 
//----------------------------------------------------------------------------------------------

export function FixedOrthoView (
	zNear = 0.1,
	zFar = 1000,
	zoomVal = 100.00,
	angleAxisX = 0, 
	angleAxisY = 0,
	screenCenter = [0,0])  //this is the very center of the screen. Units are in range -1,+1
{

	let projectionMatrix = mat4.create()
	let modelViewMatrix = mat4.create()

	this.resize = function(width,height){
		this.width = width
		this.height = height

		mat4.ortho(
			projectionMatrix,
			-width/(2*zoomVal),width/(2*zoomVal),
			-height/(2*zoomVal),height/(2*zoomVal), 
			zNear,
			zFar)

		this.updateModelView()  //it needs to be called in intialisation, but after declaration
	}

	this.set = function(gl, programInfo){
	  gl.useProgram(programInfo.program)

	  gl.uniformMatrix4fv(
	      programInfo.uniformLocations.projectionMatrix,
	      false,
	      projectionMatrix)

	  gl.uniformMatrix4fv(
	      programInfo.uniformLocations.modelViewMatrix,
	      false,
	      modelViewMatrix)
	}

	this.updateModelView = function(){ //apply transformations

		mat4.translate(
		modelViewMatrix,     		// destination matrix
		mat4.create(),    			   	// matrix to translate
		// [ 5.0, 0.0, -(zFar-zNear)/2])  	// default position
		[screenCenter[0]*this.width/(2*zoomVal), screenCenter[1]*this.height/(2*zoomVal), -(zFar-zNear)/2])

		mat4.rotateX(modelViewMatrix, modelViewMatrix , angleAxisX)
		mat4.rotateY(modelViewMatrix, modelViewMatrix , angleAxisY)
		mat4.rotateX(modelViewMatrix,modelViewMatrix,-Math.PI/2) //+Math.PI/2 to have z axis pointing upwards
	}

	//getter/setter andgleAxis:
	this.getAngleAxisX = function(){
		return angleAxisX
	}
	this.setAngleAxisX = function(ang){
		angleAxisX=ang
	}
	
	this.getAngleAxisY = function(){
		return angleAxisY
	}

	this.setAngleAxisY = function(ang){
		angleAxisY=ang
	}

	this.rotate = function(dir){
		angleAxisX -=   Math.PI * dir[1]/this.height //half
		angleAxisY -= 2*Math.PI * dir[0]/this.width
		this.updateModelView()
	}

	this.zoom = function(zoom){
		//This is a bit of a hack... but it works.
		const nzoom = zoomVal * (1 + zoom/this.height)
		if(nzoom > 0 && nzoom <= 283700){ //the limit has been experimentally determined.
			zoomVal = nzoom
			mat4.ortho(
			projectionMatrix,
			-this.width/(2*zoomVal),this.width/(2*zoomVal),
			-this.height/(2*zoomVal),this.height/(2*zoomVal), 
			zNear,
			zFar)
			this.updateModelView() //Centering is depending of zoom value, so it needs to update.
		}
		
	}

}


