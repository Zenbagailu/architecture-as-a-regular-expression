
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

function createFloatArrayBuffer(gl,vects,drawMode=gl.STATIC_DRAW){
  const vBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vects), drawMode);
  return vBuffer;
}

function createElementArrayBuffer(gl,indices,drawMode=gl.STATIC_DRAW){
  const indexBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), drawMode);
  return indexBuffer;
}


function createShadowEdgeBuffers(gl, programInfo, vertices, normals0, normals1) {
  return {
    numvertices: vertices.length,
    position: createFloatArrayBuffer(gl,vertices),
    normals0: createFloatArrayBuffer(gl,normals0),
    normals1: createFloatArrayBuffer(gl,normals1)
  };
}

function drawShadowEdgeBuffers(gl, programInfo, matrices, buffers){

  // Vertices
  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position)
  gl.vertexAttribPointer(programInfo.attribLocations.vertexPosition,4,gl.FLOAT,false,0,0)
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition)

  //Normals 0
  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.normals0)
  gl.vertexAttribPointer(programInfo.attribLocations.normal0,3,gl.FLOAT,false,0,0);
  gl.enableVertexAttribArray(programInfo.attribLocations.normal0)

   //Normals 1
  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.normals1)
  gl.vertexAttribPointer(programInfo.attribLocations.normal1,3,gl.FLOAT,false,0,0);
  gl.enableVertexAttribArray(programInfo.attribLocations.normal1)
  
  //Draw
  const vertexCount = buffers.numvertices
  matrices.forEach(mat=>{

     gl.uniformMatrix4fv(
      programInfo.uniformLocations.objectMatrix,
      false,
      mat)

    gl.drawArrays(gl.TRIANGLES, 0, vertexCount/4);//vertex has 4 components

  })
}

function createVerticesWithNormalsBuffers(gl, programInfo, vertices, normals) {

  return {
    numvertices: vertices.length,
    position:createFloatArrayBuffer(gl,vertices),
    normals: createFloatArrayBuffer(gl,normals)
  };
}


function drawVerticesWithNormals(gl, programInfo, matrices, buffers){

  // Vertices
  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.position)
  gl.vertexAttribPointer(programInfo.attribLocations.vertexPosition,3,gl.FLOAT,false,0,0)
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexPosition)

  //Normals 
  gl.bindBuffer(gl.ARRAY_BUFFER, buffers.normals)
  gl.vertexAttribPointer(programInfo.attribLocations.vertexNormal,3,gl.FLOAT,false,0,0)
  gl.enableVertexAttribArray(programInfo.attribLocations.vertexNormal)

  // Indices
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffers.indices);

  //Draw
  const vertexCount = buffers.numvertices
  matrices.forEach(mat=>{
    
     gl.uniformMatrix4fv(
      programInfo.uniformLocations.objectMatrix,
      false,
      mat)

    gl.drawArrays(gl.TRIANGLES, 0, vertexCount/3);//vertex has 3 components

  })

}



export{
  createShadowEdgeBuffers, 
  drawShadowEdgeBuffers,
  createVerticesWithNormalsBuffers,
  drawVerticesWithNormals};