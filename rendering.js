
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

import {compileProgram} from './glslprogram.js'

import {
  getTextResource, 
  getJsonResource,
  importObjFile, 
  setMeshColor, 
  calculateFaceNormals, 
  createDataForStencilEdgeShader,
  createDataForStencilCapShader,
  createDataForSmoothShader} from './utils.js'

import {
  createShadowEdgeBuffers, 
  drawShadowEdgeBuffers,
  createVerticesWithNormalsBuffers,
  drawVerticesWithNormals} from './glslbuffers.js'


export function shadowRenderableFromMesh(gl,programsInfo,mesh){

  const faceNormals = calculateFaceNormals(mesh.vertices,mesh.indices)

  const shadowEdges = createDataForStencilEdgeShader(mesh.vertices,mesh.indices, faceNormals)
  const shadowEdgeBuffer = createShadowEdgeBuffers(gl,programsInfo.edges, shadowEdges.vertices, shadowEdges.normals0, shadowEdges.normals1)

  const shadowCaps = createDataForStencilCapShader(mesh.vertices,mesh.indices, faceNormals)
  const shadowCapsBuffer = createVerticesWithNormalsBuffers(gl,programsInfo.caps,shadowCaps.vertices, shadowCaps.normals)

  const smoothed = createDataForSmoothShader(Math.PI*0.1, mesh.vertices,mesh.indices, mesh.faceNormals  === undefined ? faceNormals : mesh.faceNormals)
  const smoothBuffer = createVerticesWithNormalsBuffers(gl,programsInfo.smooth, smoothed.vertices, smoothed.normals)

  return {
    drawEdges:    function(gl,programInfo, matrices) {drawShadowEdgeBuffers(gl,programInfo,matrices,shadowEdgeBuffer)},
    drawCaps:     function(gl,programInfo, matrices) {drawVerticesWithNormals(gl,programInfo,matrices,shadowCapsBuffer)},
    drawSmooth:   function(gl,programInfo, matrices) {drawVerticesWithNormals(gl,programInfo,matrices,smoothBuffer)}}
}


export function getGL(canvas){
  const contextAttributes = {stencil: true}
  const gl = canvas.getContext('webgl',contextAttributes) || 
  canvas.getContext('experimental-webgl',contextAttributes)

  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.')
    return
  }

  //we also setup the resize here for updating the width and height in case of
  //retina displays.

  function resize(){
    //for retina displays from: https://www.khronos.org/webgl/wiki/HandlingHighDPI
    const devicePixelRatio = window.devicePixelRatio || 1;
    // set the size of the drawingBuffer based on the size it's displayed.
    canvas.width = canvas.clientWidth * devicePixelRatio
    canvas.height = canvas.clientHeight * devicePixelRatio
    gl.viewport(0, 0, canvas.width, canvas.height)
  }
  window.addEventListener("resize", () => resize())

  resize() //to update first time

  return gl;
}

export function addRedrawFunctionToEvents(canvas, renderFunction){

  //to prevent context menu from popping 
  canvas.addEventListener('contextmenu', e => {e.preventDefault()})

  canvas.addEventListener('mousedown',  renderFunction)
  canvas.addEventListener('mouseup',    renderFunction)
  canvas.addEventListener('mouseout',   renderFunction)
  canvas.addEventListener('mousemove',  renderFunction)
  canvas.addEventListener('wheel',      renderFunction)
  canvas.addEventListener('touchstart', renderFunction)
  canvas.addEventListener('touchmove',  renderFunction)
  canvas.addEventListener('touchend',   renderFunction)

}


export async function loadShadowPrograms(gl){

  //Shadow edge program
  const shadowVertexShader = await getTextResource ('./shaders/shadowEdgeVertexView.glsl')
  const shadowFragmentShader = await getTextResource ('./shaders/shadowFragment.glsl')
  const shadowEdgesProgramInfo = compileProgram(
    gl,
    shadowVertexShader,shadowFragmentShader,
    ['vertexPosition','normal0','normal1'],['objectMatrix','modelViewMatrix', 'projectionMatrix','lightDir'])

  //Shadow caps program
  const capVertexShader = await getTextResource ('./shaders/shadowCapVertexView.glsl')
  const shadowCapsProgramInfo = compileProgram(
    gl,
    capVertexShader,shadowFragmentShader,
    ['vertexPosition','vertexNormal'],['objectMatrix','modelViewMatrix', 'projectionMatrix','lightDir'])

  //Phong shading (without reflection, etc)
  const vertexShader = await getTextResource ('./shaders/smoothVertexPhong.glsl')
  const fragmentShader = await getTextResource ('./shaders/smoothFragmentPhongCut.glsl')
  const smoothProgramInfo = compileProgram(
    gl,
    vertexShader,fragmentShader,
    ['vertexPosition','vertexNormal'],['objectMatrix','modelViewMatrix', 'projectionMatrix', 'color','lightDir','ambient'])


  //we set here default object matrix values. just in case
  gl.useProgram(shadowEdgesProgramInfo.program)
  gl.uniformMatrix4fv(shadowEdgesProgramInfo.uniformLocations.objectMatrix,false,mat4.create())
  gl.useProgram(shadowCapsProgramInfo.program)
  gl.uniformMatrix4fv(shadowCapsProgramInfo.uniformLocations.objectMatrix,false,mat4.create())
  gl.useProgram(smoothProgramInfo.program)
  gl.uniformMatrix4fv(smoothProgramInfo.uniformLocations.objectMatrix,false,mat4.create())

  return {edges: shadowEdgesProgramInfo, caps: shadowCapsProgramInfo, smooth:smoothProgramInfo}
}


//it should always get an iterable, even if it contains only one object
//camera operations may be movable outside
export function shadowRendering(gl,camera, lightDir, programsInfo, shadowRenderables){
  
    //-----------------------------------------------
    //FIRST PASS: REGULAR/SMOOTH
    //-----------------------------------------------

    //Configure depth buffer
    gl.enable(gl.DEPTH_TEST)
    gl.depthFunc(gl.LEQUAL)
    gl.depthMask(true)

    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.BACK)

    //Turn off stencil buffer
    gl.disable(gl.STENCIL_TEST)

    //Turn on color write
    gl.colorMask(true, true, true, true)

    //Draw mesh with full light intensity
    camera.set(gl, programsInfo.smooth)

    gl.enable(gl.CULL_FACE); //just to speed up, not necesary
    gl.cullFace(gl.BACK);

    //set color (white)
    gl.useProgram(programsInfo.smooth.program)
    gl.uniform4fv(programsInfo.smooth.uniformLocations.color, [1,1,1,1])

    //set light ofr smoothing
    gl.uniform3fv(programsInfo.smooth.uniformLocations.lightDir,lightDir)

    //set ambient
    gl.uniform3fv(programsInfo.smooth.uniformLocations.ambient,[0.03,0.03,0.03])

    shadowRenderables.forEach(mesh=>mesh.geometry.drawSmooth(gl,programsInfo.smooth, mesh.matrices))

    //-----------------------------------------------
    //SECOND PASS: STENCILS
    //-----------------------------------------------

    gl.disable(gl.CULL_FACE);

    //Set up depth buffer
    gl.depthMask(false)
    gl.depthFunc(gl.LESS)
    
    //Set up stencil buffer
    gl.enable(gl.STENCIL_TEST)
    gl.stencilMask(0xff)
    gl.stencilFunc(gl.ALWAYS, 0, 0xff)
    gl.stencilOpSeparate(gl.BACK, gl.KEEP, gl.INCR_WRAP, gl.KEEP)
    gl.stencilOpSeparate(gl.FRONT, gl.KEEP, gl.DECR_WRAP, gl.KEEP)

    //Set up color buffer (so shadows are not actually drawn out)
    gl.colorMask(false,false,false,false)

    //an offset important when running in devices with lower depth resolution,
    //like mobiles, etc, to avoid clashes. It is good to have less otherwise,
    //to avoid wrong drawing of shadows caused by the offset.
    gl.enable(gl.POLYGON_OFFSET_FILL); 

  
    gl.polygonOffset(0, 100); //it works better with shading
   

    //Draw shadow volume

    //Edges
    camera.set(gl, programsInfo.edges)
    gl.useProgram(programsInfo.edges.program)
    gl.uniform3fv(programsInfo.edges.uniformLocations.lightDir,lightDir)

    shadowRenderables.forEach(mesh=>mesh.geometry.drawEdges(gl,programsInfo.edges, mesh.matrices))

    //Caps
    camera.set(gl, programsInfo.caps)
    gl.useProgram(programsInfo.caps.program)
    gl.uniform3fv(programsInfo.caps.uniformLocations.lightDir, lightDir)

    shadowRenderables.forEach(mesh=>mesh.geometry.drawCaps(gl,programsInfo.caps, mesh.matrices))


    //-----------------------------------------------
    //FINAL PASS: IN SHADOW
    //-----------------------------------------------
    gl.disable(gl.POLYGON_OFFSET_FILL);
    

    //Set up depth buffer
    gl.depthFunc(gl.LEQUAL)

    //Set up stencil buffer
    gl.stencilFunc(gl.NOTEQUAL, 0, 0xff)
    gl.stencilOp(gl.KEEP, gl.KEEP, gl.KEEP)

    //Turn on color mask
    gl.colorMask(true, true, true, true)

    //Draw mesh in shadow
    gl.enable(gl.CULL_FACE)
    gl.cullFace(gl.BACK)
    camera.set(gl, programsInfo.smooth)
    gl.useProgram(programsInfo.smooth.program)
  
    gl.uniform4fv(programsInfo.smooth.uniformLocations.color, [0.05,0.05,0.05,1])
    //set light ofr smoothing
    gl.uniform3fv(programsInfo.smooth.uniformLocations.lightDir,lightDir)
    //set ambient
    gl.uniform3fv(programsInfo.smooth.uniformLocations.ambient,[0.03,0.03,0.03])
    
    shadowRenderables.forEach(mesh=>mesh.geometry.drawSmooth(gl,programsInfo.smooth, mesh.matrices))

    //reset stencil
    gl.clearStencil(0)
    gl.clear(gl.STENCIL_BUFFER_BIT)

  }
