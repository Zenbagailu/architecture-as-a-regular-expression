
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

async function getTextResource(url){
  try{
    const response = await fetch(url)
    const data = await response.text()
    return data
  }catch(err){
    console.error(err)
  } 
}

async function getJsonResource(url){
  try{
    const response = await fetch(url)
    const data = await response.json()
    return data
  }catch(err){
    console.error(err)
  } 
}

async function importObjFile(url){
  const text = await getTextResource(url)
  const out = {meshes: [] }
  const messages ={
    extraVertexData: null, 
    quads: null,
    extraFacesData: null,
    normals: null,
    textureCoords:null,
    parameterSV: null
  }

  function Mesh(name){
    this.name = name === undefined ? 'mesh': name
    this.vertices = []
    this.colors = []
    this.indices = []
  }
  
  let current = null
  let voffset = 1
  let vertexCount = 0

  text.split('\n').map(line => {
    const tokens=line.split(' ')
    switch (tokens[0]){
    case 'o': //new object
    out.meshes.push(new Mesh(tokens[1]))
    current =  out.meshes[out.meshes.length-1] //set the last one as current
    voffset = vertexCount + 1 //meshes have indices that are global to the vertices
    break

    case 'v': //vertex
    if(tokens.length> 4 && !messages.extraVertexData ){ 
      messages.extraVertexData = 'Vertices with extra data (possibly color or a w coord) detected. Only processing 3 values of geometry.'
    }
    if(!current){ //the file did not use 'o', it has only geometry
      out.meshes.push(new Mesh(tokens[1]))
      current =  out.meshes[out.meshes.length-1] //set the last one as current
    }
    
    ++vertexCount
   current.vertices.push(...tokens.slice(1,4).map(parseFloat))
    break

    case 'f': //face
    if(tokens.length> 4 && !messages.quads){ 
      messages.quads = 'Faces with more than three vertices detected, only three will be processed. Make sure to use triangular faces.'
    }
    const separator = /\//
    if(separator.test(tokens[1]) && !messages.extraFacesData){
      messages.extraFacesData = 'Faces with references to texture or normal data detected. Only geometry is processed at the moment.'
    }
    const untilFirst = /[^\/]*/
    current.indices.push(...tokens.slice(1,4).map(token => parseInt(untilFirst.exec(token)[0])-voffset)) //indices start at 1
    break

    case 'vn': //vertex normal 
    if(!messages.normals){
      messages.normals = 'Vertex normals found, they will not be processed.'
    }
    break
    case 'vt': //texture coordinate
    if(!messages.textureCoords){
      messages.textureCoords = 'Texture Coordinates found, they will not be processed.'
    }
    break

    case 'vp': //parameter spaces vertices
    if(!messages.parameterSV){
      messages.parameterSV = 'Parameter space vertices found, they will not be processed.'
    }
    break
    }
  })

  //Print messages
  Object.keys(messages).forEach((key, index) => { 
    if(messages[key])
    console.log(messages[key])
  })

  return out
}

function setMeshColor(mesh, color){
  if(color.length===3){
    color.push(1.0)
  }
  else if (color.length<3){
    console.error('wrong color description in setMeshColor(), it needs to have at least 3 components.')
  } 

  mesh.colors = Array.from({length: (mesh.vertices.length/3)*4}, (_, i) => color[i%4])
}


function calculateNormal(v0,v1,v2){
  // console.log(`${v0}, ${v1}, ${v2}`)
  const u = v0.map((c,i)=>v1[i]-c)
  const v = v0.map((c,i)=>v2[i]-c)
  const n =  [
  u[1]*v[2] - u[2]*v[1],
  u[2]*v[0] - u[0]*v[2],
  u[0]*v[1] - u[1]*v[0],
  ]
  //normalise
  const dim = Math.sqrt(n[0]*n[0] + n[1]*n[1] + n[2]*n[2])
  return n.map(x => x/dim)
}

function calculateFaceNormals(vertices,faceIndices){
  const normals = []
  for(let fInd = 0; fInd<faceIndices.length; fInd += 3){
    const v0ind = 3*faceIndices[fInd], v1ind = 3*faceIndices[fInd+1],  v2ind = 3*faceIndices[fInd+2]
    const normal = calculateNormal(
      [vertices[v0ind  ],vertices[v0ind+1],vertices[v0ind+2]],
      [vertices[v1ind  ],vertices[v1ind+1],vertices[v1ind+2]],
      [vertices[v2ind  ],vertices[v2ind+1],vertices[v2ind+2]])
    normals.push(...normal)
  }
  return normals
}

function createDataForStencilEdgeShader(vertices, faceIndices, faceNormals){
  //Create edges with faces:
  const edges ={}
  for(let fInd=0; fInd<faceIndices.length; fInd+=3){ //face index is to the position of the first vertex of the face
    for(let j=0;j<3;++j){
      let indV0 = faceIndices[fInd+j]
      let indV1 = faceIndices[fInd+(j+1)%3]
      let offace = 'f1'
      if(indV0>indV1){
        [indV0, indV1] = [indV1, indV0] //swap so first is always lower (to find in object)
        offace = 'f0' // f0 is with winding against v0->v1 (see explanation in shader, shadowEdgeVertex.glsl)
      }
      var key= indV0 + ' ' + indV1

      if(!(key in edges)){
        edges[key] = {v0:indV0, v1:indV1, f0: null, f1: null }
      }
       edges[key][offace] = fInd
    }
  }

  if(!faceNormals){
    faceNormals = calculateFaceNormals(vertices,faceIndices)
    // console.log(faceNormals)
  }

  const edgeVerts = []
  const norm0 = []
  const norm1 = []

  for(let eId in edges){
    const edge = edges[eId]
    const v0 = [vertices[3*edge.v0  ], vertices[3*edge.v0+1], vertices[3*edge.v0+2], 1] //observe the w component
    const v1 = [vertices[3*edge.v1  ], vertices[3*edge.v1+1], vertices[3*edge.v1+2], 1]
  
    //this is to deal with fragments of meshes that will be closed, but do not represent a full volume
    //these fragments should be made so they meet other faces in adjacent fragments which are on the same plane, 
    //that is, they have the same normal. Otherwise the assumption below does not work.
   
    //If an open edge, assume that the face missing, which is in an adjacent  mesh, has the same normal
    const n0 = edge.f0 !== null? 
    [faceNormals[edge.f0],  faceNormals[edge.f0+1], faceNormals[edge.f0+2]]:
    [faceNormals[edge.f1],  faceNormals[edge.f1+1], faceNormals[edge.f1+2]]

    const n1 =  edge.f1 !== null? 
    [faceNormals[edge.f1],  faceNormals[edge.f1+1], faceNormals[edge.f1+2]]:
    [faceNormals[edge.f0],  faceNormals[edge.f0+1], faceNormals[edge.f0+2]]

    // check the shadow edge vertex for the reason for the ordering
    // v0, n0,n1, v1,n0,n1, 0, n0, n1,    v1, n1,n0,  v0, n1,n0, 0 , n1,n0,
    edgeVerts.push(...v0)
    norm0.push(...n0)
    norm1.push(...n1)

    edgeVerts.push(...v1)
    norm0.push(...n0)
    norm1.push(...n1)

    edgeVerts.push(...[0,0,0,0])
    norm0.push(...n0)
    norm1.push(...n1)

    edgeVerts.push(...v1)
    norm0.push(...n1)
    norm1.push(...n0)

    edgeVerts.push(...v0)
    norm0.push(...n1)
    norm1.push(...n0)

    edgeVerts.push(...[0,0,0,0])
    norm0.push(...n1)
    norm1.push(...n0)
  }

  return { vertices: edgeVerts, normals0: norm0, normals1: norm1}
}

//normals have to correspond to the faces in faceIndices
function createDataForStencilCapShader(vertices, faceIndices, faceNormals){
  const outVerts=[]
  const outNorms=[]

   for(let fInd=0; fInd<faceIndices.length; fInd+=3){ //face index is to the position of the first vertex of the face

      const v0Ind = faceIndices[fInd  ]
      const v1Ind = faceIndices[fInd+1]
      const v2Ind = faceIndices[fInd+2]
      const v0 = [vertices[3*v0Ind  ], vertices[3*v0Ind+1], vertices[3*v0Ind+2]]
      const v1 = [vertices[3*v1Ind  ], vertices[3*v1Ind+1], vertices[3*v1Ind+2]]
      const v2 = [vertices[3*v2Ind  ], vertices[3*v2Ind+1], vertices[3*v2Ind+2]]

      let normal = !faceNormals ? calculateNormal(v0,v1,v2) : [faceNormals[fInd],faceNormals[fInd+1],faceNormals[fInd+2]]

      outVerts.push(...v0)
      outNorms.push(...normal)

      outVerts.push(...v1)
      outNorms.push(...normal)

      outVerts.push(...v2)
      outNorms.push(...normal)
  } 
  return {vertices: outVerts, normals: outNorms}
}

//normals have to correspond to the faces in faceIndices
function createDataForSmoothShader(thresehold, vertices, faceIndices, faceNormals){
  const outVerts=[]
  const tempNorms=[]
  const adjacentVs= Array.from({length: vertices.length/3}, () => [])


   for(let fInd = 0; fInd<faceIndices.length; fInd += 3){ //face index is to the position of the first vertex of the face

      const v0Ind = faceIndices[fInd  ]
      const v1Ind = faceIndices[fInd+1]
      const v2Ind = faceIndices[fInd+2]
      const v0IndPos = 3*v0Ind, v1IndPos = 3*v1Ind, v2IndPos = 3*v2Ind
      const v0 = [vertices[v0IndPos  ], vertices[v0IndPos+1], vertices[v0IndPos+2]]
      const v1 = [vertices[v1IndPos  ], vertices[v1IndPos+1], vertices[v1IndPos+2]]
      const v2 = [vertices[v2IndPos  ], vertices[v2IndPos+1], vertices[v2IndPos+2]]

      let normal = !faceNormals ? calculateNormal(v0,v1,v2) : [faceNormals[fInd],faceNormals[fInd+1],faceNormals[fInd+2]]


      outVerts.push(     ...v0,    ...v1,    ...v2)
      tempNorms.push(...normal,...normal,...normal)

      //the index of the normals of the faces adjacent to each vertex, which is really the index of the vertex in the 
      //new triangles in outVerts, and correspond also to tempNorms
      adjacentVs[v0Ind].push(3*fInd) 
      adjacentVs[v1Ind].push(3*(fInd+1))
      adjacentVs[v2Ind].push(3*(fInd+2))
  } 

  const outNorms=[...tempNorms]

  //now we smooth them:
  const limCos = Math.cos(thresehold)

  //This does averaging, but it does not work, because it is weighted against the number of faces adjacent to the vertex. 
  //For example, if 5 faces with the same normal are adjacent, they will weight more than one or two with a different normal.
  //That results in artefacts in the shading.

  adjacentVs.forEach(adj =>{ //for all the vertices corresponding to an original vertex
    adj.forEach(vaPos =>{ //for each vertex copy corresponding to an original vertex
      const norma=tempNorms.slice(vaPos,vaPos+3) //get its corresponding stored normal
      const relevantNormals = [norma]

      adj.forEach(vbPos =>{ //for all others 
        if(vaPos !== vbPos){
          const normb=tempNorms.slice(vbPos,vbPos+3)
          const angCos = vec3.dot(norma, normb)
          if(angCos>limCos){ //if it is within the limit to consider
            if(relevantNormals.every(rn=> vec3.dot(rn, normb) < 0.9999)){ //if none are  parallel
              relevantNormals.push(normb)
            }
          }
        }
      })

      let result = [0,0,0]
      relevantNormals.forEach(n=> result = result.map((r,i)=> r+n[i]))
      outNorms[vaPos  ] = result[0]/result.length
      outNorms[vaPos+1] = result[1]/result.length
      outNorms[vaPos+2] = result[2]/result.length

    })
  })


  return {vertices: outVerts, normals: outNorms}
}


export {
  getTextResource,
  getJsonResource,
  importObjFile, 
  setMeshColor, 
  calculateNormal,
  calculateFaceNormals, 
  createDataForStencilEdgeShader,
  createDataForStencilCapShader,
  createDataForSmoothShader}
