
/*
================================================================================
Copyright (c) 2021 Pablo Miranda Carranza

MIT License


Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
================================================================================
*/

//import {Renderer} from '/renderer.js'

import {
  getGL,
  addRedrawFunctionToEvents,
  loadShadowPrograms,
  shadowRenderableFromMesh,
  shadowRendering} from './rendering.js'

import {
  PerspectiveView,
  OrthoView,
  Navigation3D,
  FixedPerspective,
  FixedOrthoView} from './views.js'

// import {importObjFile} from '/utils.js'
import {
  generateElementMeshes,
  makeBox,
  makeTruncatedPyramid} from './solids.js'

//glMatrix.setMatrixArrayType(Float32Array) //default

function binaryStringToGrid(states,w,h){ //states is a number converted to a binary string
  states = states.length < w*h ? '0'.repeat(w*h-states.length) + states : states
  // console.log(`${number} : ${states}`);
  let pos = 0
   return Array.from({length: h}, () => 
    Array.from({length: w}, () =>{ 
      const val = states[pos] ==='1' ?'t':'f'
      ++pos
      return val
    })
  )
}


function interpretelements(raster,drawingFunction){ 
  const cases = {
    'ffff' : [undefined,0],
    'tfff' : ['singleCorner',0],
    'ftff' : ['singleCorner',0.5],
    'ttff' : ['consecutiveCorners',0.5],
    'fftf' : ['singleCorner',1.0],
    'tftf' : ['twoOppositeCorners',0],
    'fttf' : ['consecutiveCorners',1.0],
    'tttf' : ['threeCorners',1.0],
    'ffft' : ['singleCorner',1.5],
    'tfft' : ['consecutiveCorners',0.0],
    'ftft' : ['twoOppositeCorners',0.5],
    'ttft' : ['threeCorners',0.5],
    'fftt' : ['consecutiveCorners',1.5],
    'tftt' : ['threeCorners',0.0],
    'fttt' : ['threeCorners',1.5],
    'tttt' : ['fourCorners',0]
  }

  const h = raster.length, w = raster[0].length

  for(let y=-1; y<h; ++y){
    for(let x=-1; x< w; ++x){

      const key= ''.concat(
        y >= 0 && x+1 < w  ?  raster[y  ][x+1] :'f',
        x >= 0 && y >= 0   ?  raster[y  ][x  ] :'f',
        x >= 0 && y+1 < h  ?  raster[y+1][x  ] :'f',
        x+1 < w && y+1 < h ?  raster[y+1][x+1] :'f')

      const type=cases[key]
      drawingFunction(type[0],type[1],x,y)
    }
  }
}

function numberToBinaryString(number,stringLen){
  let binaryString = number.toString(2)
  return binaryString.length < stringLen ? '0'.repeat(stringLen-binaryString.length) + binaryString : binaryString
}


//apply a CA-like rule to the top
function ruleGridTransform(grid){
  const neigs=[
      [-1,-1],[ 0,-1],[ 1,-1],
      [-1, 0]        ,[ 1, 0],
      [-1, 1],[ 0, 1],[ 1, 1]
  ]

  const newGrid=[]

  const gh=grid.length
  const gw= gh > 0 ? grid[0].length : 0

  for(let y = 0 ; y < gh; ++y){
     const newRow=[]
    for(let x = 0 ; x < gw; ++x){
      let neigct = 0
      for(let neig of neigs){
        const neigRow = grid[y+neig[1]]
        const neigState = neigRow === undefined ? undefined : neigRow[x+neig[0]]
        neigct += neigState === undefined ? 0 : neigState === 't' ? 1 : 0 //if out of bounds it will be undefined.
      }
      newRow.push(grid[y][x] =='t' || neigct>=3? 't' : 'f') //CA-like rule

    }
     newGrid.push(newRow)
  }
  return newGrid
}


function createSvgPolygons(segments=10){
  const angStep = Math.PI/(2*segments)
  const rad = 1.0

  function ptsToString(pts){
    return pts.map(pt=>pt.join(',')).join(' ') 
  }

  //Positives...
  const curve=[]
  for(let i=0; i <= segments; ++i){
    curve.push([
      rad*Math.cos(i*angStep)-1.0,
      rad*Math.sin(i*angStep)-1.0
      ])
  }

  const single1  = ptsToString([[-1,-1]].concat(curve))
  const single2  = ptsToString([[ 1, 1]].concat(curve.map( pt => [-pt[0],-pt[1]])))
  const halfRect = ptsToString([[-1,-1],[ 1,-1],[ 1, 0],[-1, 0]])
  const square   = ptsToString([[-1,-1],[-1, 1],[ 1, 1],[ 1,-1]])
  const composed = ptsToString([[-1,-1],[ 1,-1],[ 1, 1]].concat(curve.map( pt=> [pt[0],-pt[1]])))

  return {
    singleCorner:[single1], 
    consecutiveCorners:[halfRect],
    threeCorners: [composed],
    twoOppositeCorners: [single1,single2],
    fourCorners: [square]}
}



function createSvgOutlines(segments=10){
  const angStep = Math.PI/(2*segments)
  const rad = 1.0

  function ptsToString(pts){
    return pts.map(pt=>pt.join(',')).join(' ') 
  }

  //Positives...
  const curve=[]
  for(let i=0; i <= segments; ++i){
    curve.push([
      rad*Math.cos(i*angStep)-1.0,
      rad*Math.sin(i*angStep)-1.0
      ])
  }

  const single1  = ptsToString(curve)
  const single2  = ptsToString(curve.map( pt => [-pt[0],-pt[1]]))
  const halfRect = ptsToString([[-1, 0],[ 1, 0]])
  const square   = ptsToString([]) //nothing
  const composed = ptsToString(curve.map( pt=> [pt[0],-pt[1]]))

  return {
    singleCorner:[single1], 
    consecutiveCorners:[halfRect],
    threeCorners: [composed],
    twoOppositeCorners: [single1,single2],
    fourCorners: [square]}
}


async function main() {

  // let number = 1 //initial number
  const h = 4
  const w = 5
  let binaryString; // = numberToBinaryString(1,w*h)

  let animated = true
  let animationStarted = false

  //for a view aligned light, the variables are x and z
  let lightDir = [ 0, 0.75, -1] //default value
  // let lightDir = [] //default value

  //Interface
  const ligthtPosSlider = document.getElementById('lightDirectionSlider');

  function computeLightDir() {
    const ang = Math.PI*((2/3)*(ligthtPosSlider.value-ligthtPosSlider.min)/(ligthtPosSlider.max-ligthtPosSlider.min) + 1/6)
    const x = Math.cos(ang)
    const z = Math.sin(ang)
    return [-x,-0.5,-z] //for ortho
  }

  if(ligthtPosSlider){
    lightDir = computeLightDir() //recalculate according to slider value
   
    ligthtPosSlider.addEventListener('input',() => {
      lightDir = computeLightDir()
      if(!animated){
        requestAnimationFrame(renderFunction)
      }
    })
  }
  else{
    lightDir = [-0.7, -0.5, -0.7]
  }

  //Start stop animation
  const animationButton = document.getElementById('animationButton')
  if(animationButton){
    animationButton.innerHTML = animated ? 'Stop.' : 'Count.' //set default value at start
    animationButton.addEventListener('click',() => {
      animated = !animated //toggle
      animationStarted = !(animationStarted && animated)  //if it had already started, and it has been switched to animated,, marked as not started
      animationButton.innerHTML = animated ? 'Stop.' : 'Count.'
      //it needs to request a frame in both cases, to either start the animation or update the screen
      calculateTranslation(binaryString)
      requestAnimationFrame(renderFunction)
    });
  }

  //Variable to keep track of which input is triggering redraws.
  let trigger 
  
  //Display and set the number 
  const numberField = document.getElementById('number')

  //Prevent anything but numbers to be written. 
  numberField.onkeypress = (event)=>{ 
    const charCode= event.charCode
    if(/[^\d]/.test(String.fromCharCode(charCode))){
      return false
    }
    const valueString = numberField.value
    if(valueString.length > Math.pow(2,w*h).toString().length){
      return false
    }

    const futureVal = parseInt(valueString + String.fromCharCode(charCode))
    if (futureVal > Math.pow(2,w*h)){
      return false
    }

    return true
  };
  // Prevent pasting (since pasted content might include non-number characters)
  numberField.onpaste = () => false;

  numberField.addEventListener('keyup', ()=>{
    binaryString = numberToBinaryString(Number(numberField.value),w*h)

    //now update all the visuals
    trigger = numberField
    calculateTranslation(binaryString)
    if(!animated){
       requestAnimationFrame(renderFunction)
    }
    trigger = undefined

  })

  function outputNumber(num){
    if(numberField){
      // store current positions in variables
      const start = numberField.selectionStart
      const end = numberField.selectionEnd
      numberField.value = num
      if (trigger===numberField){
        numberField.setSelectionRange(start, end);
      }
      
    }
  }
  //Set the total
  const totalV = document.getElementById('total')

  //used for outputting number as a binary string
  const binaryOut = document.getElementById('binaryString')
  binaryOut.style.width='100%'
  
  binaryOut.onkeypress = (event)=>{ 
    const charCode= event.charCode
    if(/[^0-1]/.test(String.fromCharCode(charCode))){
      return false
    }
    return true
  }
  // Prevent pasting (since pasted content might include non-number characters)
  binaryOut.onpaste = () => false

  binaryOut.addEventListener('keyup', ()=>{
    binaryString = binaryOut.value
    //now update all the visuals
    //see if it needs filling with heading zeros or it needs to be cropped:
     binaryString = binaryString.length < w*h ? '0'.repeat((w*h)-binaryString.length) + binaryString : binaryString
     binaryString = binaryString.length > w*h ?  binaryString.slice(binaryString.length-(w*h)) : binaryString

    console.log(binaryString)

    trigger = binaryOut
    calculateTranslation(binaryString)
    if(!animated){
       requestAnimationFrame(renderFunction)
    }
    trigger = undefined
  
  })

  function outputBinaryString(binaryString){
    if(binaryOut){
      // store current positions in variables. Because the start
      //and end count from the left, and that may change, we need 
      //to specify to do it from right (backward)
      const len = binaryOut.value.length
      const start = len - binaryOut.selectionStart
      const end = len - binaryOut.selectionEnd
      binaryOut.value = binaryString  
      // restore from variables...
      const nlen = binaryOut.value.length
      binaryOut.setSelectionRange(nlen-start, nlen-end, 'backward');
    }
  }


  const svgGrid = document.getElementById('SvgGrid')
  let svgGridData = {}
  let svgPolygons
  let svgOutlines

  function intialiseSvgOut(){
    svgGridData.svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    svgGridData.svg.setAttribute('width', '100%')
    svgGridData.svg.setAttribute('height', '100%')
    svgGrid.appendChild(svgGridData.svg)

    svgPolygons = createSvgPolygons()
    svgOutlines = createSvgOutlines()

    svgGridData.svgForms = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    svgGridData.svg.appendChild(svgGridData.svgForms)

    svgGridData.svgBase = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    svgGridData.svg.appendChild(svgGridData.svgBase)

    svgGridData.svgText = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    svgGridData.svg.appendChild(svgGridData.svgText)

    svgGridData.svgOverlay = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    svgGridData.svg.appendChild(svgGridData.svgOverlay)


    //create overlay 
    const svg = svgGridData.svg
    svgGridData.elemSize = Math.floor(w > h ? 
    (svg.clientWidth/w)*h <= svg.clientHeight ? svg.clientWidth/w : svg.clientHeight/h : 
    (svg.clientWidth/h)*w <= svg.clientWidth ? svg.clientHeight/h : svg.clientWidth/w)

    svgGridData.svg.setAttribute('viewBox', `-10 0 ${svg.clientWidth+20} ${svg.clientHeight}`)


    svgGridData.svgOverlay.setAttribute('stroke-width', 1)
    svgGridData.svgOverlay.setAttribute('stroke','#8F8F8F')

    const crossSize = svgGridData.elemSize*0.1
    for(let y=0; y<h; ++y){
      for(let x=0; x<w; ++x){
        const lineH = document.createElementNS('http://www.w3.org/2000/svg','line');
        lineH.setAttribute('x1',`${(0.5 + x)*svgGridData.elemSize-crossSize}`);
        lineH.setAttribute('y1',`${(0.5 + y)*svgGridData.elemSize}`);
        lineH.setAttribute('x2',`${(0.5 + x)*svgGridData.elemSize+crossSize}`);
        lineH.setAttribute('y2',`${(0.5 + y)*svgGridData.elemSize}`);
        // lineH.setAttribute("stroke", "#202020")
        svgGridData.svgOverlay.appendChild(lineH)

        const lineV = document.createElementNS('http://www.w3.org/2000/svg','line');
        lineV.setAttribute('x1',`${(0.5 + x)*svgGridData.elemSize}`);
        lineV.setAttribute('y1',`${(0.5 + y)*svgGridData.elemSize-crossSize}`);
        lineV.setAttribute('x2',`${(0.5 + x)*svgGridData.elemSize}`);
        lineV.setAttribute('y2',`${(0.5 + y)*svgGridData.elemSize+crossSize}`);
        // lineV.setAttribute("stroke", "#202020")
        svgGridData.svgOverlay.appendChild(lineV)
      }
    }

    //Add event listener:
    const rect = svgGrid.getBoundingClientRect();
    svgGrid.addEventListener("mousedown",  e=>{
      const px = Math.floor((e.clientX - rect.left)/svgGridData.elemSize)
      const py = Math.floor((e.clientY - rect.top )/svgGridData.elemSize)
      const pos = py*w + px
      binaryString = binaryString.slice(0,pos) + (binaryString[pos] === '1' ? '0' : '1') + binaryString.slice(pos+1)
      calculateTranslation(binaryString)
      if(!animated){
        requestAnimationFrame(renderFunction)
      }   
    })

  }

  function intialiseSvgLayer(layer, attributes){
    while (layer.lastElementChild){
      layer.removeChild(layer.lastElementChild);
    }
    Object.entries(attributes).forEach(([key,value])=>layer.setAttribute(key,value))
  }

  function outputGridAsSvg(layer,grid,geometryDesc,svgType){
    interpretelements(grid,(type,rotation,x,y)=>{
      if(geometryDesc[type]){
        for(let pts of geometryDesc[type]){
          const shape = document.createElementNS('http://www.w3.org/2000/svg', svgType);
          shape.setAttribute('points', pts)
          shape.setAttribute('transform',`scale(${svgGridData.elemSize/2}) translate(${(1+x)*2},${(1+y)*2}) rotate(${90-rotation*180})`)
          layer.appendChild(shape)
        }
      }
    })  
  }

  function outputSvgText(layer, grid){
    grid.forEach((row,y)=>row.forEach((v,x)=>{
      const label =  document.createElementNS('http://www.w3.org/2000/svg','text')
      label.setAttribute('x',`${(0.6+x)*svgGridData.elemSize}`)
      label.setAttribute('y',`${(0.42+y)*svgGridData.elemSize}`)
      label.setAttribute('fill', v === 't' ? '#FDFDFD' : '#202020')
      label.appendChild(document.createTextNode(v === 't' ? '1' : '0'))
      layer.appendChild(label)
    }))   
  }



  let lastRecalc
  let lastRotate

  //initialisation on load for elements that need it
  window.addEventListener('load', ()=>{
    if(numberField){
      numberField.style.width = (Math.pow(2,w*h).toString().length*0.6).toString() + 'em' 

    }
    if(totalV){
      totalV.innerHTML = '/ ' + (Math.pow(2,w*h)-1).toString()
    }
    if(binaryOut){
      //here binaryOut can be initialised if necessary

    }

    //This is to see if states were saved. Otherwise initialise binaryString and store the value.

    if(!localStorage.getItem('binaryString')){
      binaryString = numberToBinaryString(1,w*h)
      localStorage.setItem('binaryString', binaryString)
    } 
    else{
      binaryString = localStorage.getItem('binaryString')
    }

    if(svgGrid){
      intialiseSvgOut()
    }

    camera = createInteractiveCamera()
  })


  function createInteractiveCamera(){
    const devicePixelRatio = window.devicePixelRatio || 1;
    const interLayout = document.getElementById('interfaceLayout')
   
    const interWidth= interLayout.clientWidth*devicePixelRatio //(canvas width is already scaled in rendering.js, resize())
    const posx = 2*(interWidth + (canvas.width - interWidth)/2)/canvas.width - 1

    const camera = new FixedOrthoView(0.1,500, 115.00,  Math.PI/4, Math.PI+Math.PI/4,[posx,-0.15,0])

    // set the size of the drawingBuffer based on the size it's displayed.
  
    camera.resize(canvas.width, canvas.height) //needed to set sizes correct

    window.addEventListener("resize", () => {
      camera.resize(canvas.width, canvas.height)
      if(!animated){
         requestAnimationFrame(renderFunction)
      }
    })

    const navigation = new Navigation3D(camera) //later

    navigation.addListenersTo(canvas)


    //These add extra functionnality to manipulate the camera rotation
    if(FixedOrthoView.prototype.rotateAxisX === undefined ){  
      FixedOrthoView.prototype.rotateAxisX = function(degrees){
        this.setAngleAxisX(this.getAngleAxisX() + Math.PI * degrees/180)
        this.updateModelView()
      }
    }

    if(FixedOrthoView.prototype.rotateAxisY === undefined ){
      FixedOrthoView.prototype.rotateAxisY = function(degrees){
        this.setAngleAxisY(this.getAngleAxisY() + Math.PI * degrees/180)
        this.updateModelView()
      }
    }

    return camera

  }

  // const canvas = document.querySelector('#glcanvas')
  const canvas = document.getElementById('glcanvas')
  const gl = getGL(canvas)
  let camera
  // const camera = createInteractiveCamera()


  const programsInfo= await loadShadowPrograms(gl)
  
  const meshGeomTop = generateElementMeshes([[0.75,0],[0.75,0.5],[1.0,0.5],[1.0,5.0],[0.75,5.0],[0.75,4.75]],10) //<--Small border top

  const bufferGeometriesTop ={
    singleCorner: shadowRenderableFromMesh(gl,programsInfo,meshGeomTop.singleCorner),
    consecutiveCorners:shadowRenderableFromMesh(gl,programsInfo,meshGeomTop.consecutiveCorners),
    threeCorners: shadowRenderableFromMesh(gl,programsInfo,meshGeomTop.threeCorners),
    twoOppositeCorners: shadowRenderableFromMesh(gl,programsInfo,meshGeomTop.twoOppositeCorners),
    fourCorners:shadowRenderableFromMesh(gl,programsInfo,meshGeomTop.fourCorners)
  }

  //Base Geom
  const meshGeomBase = generateElementMeshes([[1.0, -2.0],[1.0, 0.0]],10) //<--Basic

  const bufferGeometriesBase ={
    singleCorner: shadowRenderableFromMesh(gl,programsInfo,meshGeomBase.singleCorner),
    consecutiveCorners:shadowRenderableFromMesh(gl,programsInfo,meshGeomBase.consecutiveCorners),
    threeCorners: shadowRenderableFromMesh(gl,programsInfo,meshGeomBase.threeCorners),
    twoOppositeCorners: shadowRenderableFromMesh(gl,programsInfo,meshGeomBase.twoOppositeCorners),
    fourCorners:shadowRenderableFromMesh(gl,programsInfo,meshGeomBase.fourCorners)
  }


  const cx = w - 2
  const cy = h - 2
 
  let elements


  const calculateTranslation = function(binaryString){
      let grid = binaryStringToGrid(binaryString,w,h)
      let baseGrid = ruleGridTransform(grid)
      elements =[]

      //this is the top
      //temporary to store translations associated with types
      const topElems = {
        singleCorner: [],
        consecutiveCorners: [],
        threeCorners:[],
        twoOppositeCorners:[],
        fourCorners: []
      }

      interpretelements(grid,(type,rotation,x,y)=>{
        if(bufferGeometriesTop[type]){
          topElems[type].push(
            mat4.rotateZ(
              mat4.create(),
              mat4.fromTranslation(mat4.create(), [-x*2.0 + cx,y*2.0-cy,0]),
              Math.PI*rotation))
        } 
      })

      //now associate transformations with mesh geometries
       elements.push(...Object.entries(topElems).map(([type,trans])=>({geometry: bufferGeometriesTop[type], matrices: trans})))

       const botElems = {
        singleCorner: [],
        consecutiveCorners: [],
        threeCorners:[],
        twoOppositeCorners:[],
        fourCorners: []
      }

      interpretelements(baseGrid,(type,rotation,x,y)=>{
        if(bufferGeometriesBase[type]){
          botElems[type].push(
            mat4.rotateZ(
              mat4.create(),
              mat4.fromTranslation(mat4.create(), [-x*2.0 + cx,y*2.0-cy,0]),
              Math.PI*rotation))
        } 
      })
      elements.push(...Object.entries(botElems).map(([type,trans])=>({geometry: bufferGeometriesBase[type], matrices: trans})))
 

      outputNumber(parseInt(binaryString, 2))
      outputBinaryString(binaryString)
      // outputTextAsGrid(binaryString)

      //svg
      if(svgGridData.svg){
        //main grid
        intialiseSvgLayer(svgGridData.svgForms,{'fill':'#0F0F0F','stroke': '#040404','stroke-width': 0.03})
        outputGridAsSvg(svgGridData.svgForms,grid,svgPolygons,'polygon')
        //base

        intialiseSvgLayer(svgGridData.svgBase,{'fill':'none','stroke': '#404040','stroke-width': 0.03,'stroke-opacity': '0.7'})
        outputGridAsSvg(svgGridData.svgBase,baseGrid,svgOutlines,'polyline')
        //text
        intialiseSvgLayer(svgGridData.svgText,{'stroke': 'none','text-anchor':'middle'})
        outputSvgText(svgGridData.svgText,grid)
      }
      
  }


  const renderFunction = function(timestamp){
    if(animated && (lastRecalc === undefined || timestamp-lastRecalc >= 1000)){ //half seconds, it will always run the first time
      lastRecalc = timestamp
      calculateTranslation(binaryString)
      let number = parseInt(binaryString, 2) + 1
      number = number <  Math.pow(2,w*h) ? number : 1
      binaryString = numberToBinaryString(number,w*h)
      localStorage.setItem('binaryString', binaryString)
    }

    if(animated){
      if(!animationStarted){
        console.log('restarting rotation.')
        lastRotate = timestamp
        animationStarted = true
      }
      // camera.rotateAxisY(0.1)
      camera.rotateAxisY((timestamp-lastRotate)*0.0075)
      lastRotate = timestamp
    }



    //basic setup, possibly here.
    gl.clearColor(0.98,0.98,0.98,1)
    gl.clear(
      gl.COLOR_BUFFER_BIT |
      gl.DEPTH_BUFFER_BIT)
    shadowRendering(gl,camera,lightDir,programsInfo,elements)  
      
    if(animated){
      requestAnimationFrame(renderFunction)
    } 
  }

  addRedrawFunctionToEvents(canvas, ()=> {if(!animated){requestAnimationFrame(renderFunction)}});
  //quick off the rendering 
  lastRotate=0
  requestAnimationFrame(renderFunction)
}

main()








